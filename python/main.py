import json
import socket
import sys

class Position:
    def __init__(self, data):
        self.name = data["id"]["name"]
        self.angle = data["angle"]
        self.pieceIndex = data["piecePosition"]["pieceIndex"]
        self.inPieceDistance = data["piecePosition"]["inPieceDistance"]
        self.startLane = data["piecePosition"]["lane"]["startLaneIndex"]
        self.startLane = data["piecePosition"]["lane"]["endLaneIndex"]

class Piece :
    def __init__(self, data):
        if "length" in data:
            self.type = "straight"
            self.length = data["length"]
        else:
            self.type = "round"
            self.radius = data["radius"]
            self.angle = data["angle"]

        self.switch = ("switch" in data) and (data["switch"])


class Track :
    def __init__(self, data):
        self.id = data["id"]
        self.name = data["name"]
        self.pieces = map(Piece, data["pieces"])
        self.npieces = len(self.pieces)
        self.lanes = data["lanes"]
        self.nlanes = len(self.lanes)

    def is_straight(self, ind):
        return self.pieces[ind].type == "straight"
    
    def is_round(self, ind):
        return self.pieces[ind].type == "round"

    def next_round_piece(self, ind):
        i = ind
        while self.pieces[i].type == "straight":
            i += 1
            i = i % self.npieces
        return i

class DecentBot(object):

    def __init__(self, socket, name, key):
        self.socket = socket
        self.name = name
        self.key = key
        self.last_tick = 0
        self.last_position = 0
        self.cur_throttle = 0.6

    def msg(self, msg_type, data):
        self.send(json.dumps({"msgType": msg_type, "data": data}))

    def send(self, msg):
        self.socket.sendall(msg + "\n")

    def join(self):
        return self.msg("join", {"name": self.name,
                                 "key": self.key})

    def throttle(self, throttle):
        self.msg("throttle", throttle)

    def switch_lane(self, lane):
        self.msg("switchLane", lane)

    def ping(self):
        self.msg("ping", {})

    def run(self):
        self.join()
        self.msg_loop()

    def on_join(self, msgType, data, **kwargs):
        print("Joined")
        self.ping()

    def on_game_start(self, data, **kwargs):
        print("Race started")
        self.ping()

    def on_car_positions(self, msgType, data, gameId, **kwargs):
        self.positions = dict(map(lambda x: (x["id"]["name"], Position(x)), data))
        game_tick = self.last_tick + 1 
        position = self.positions[self.name].inPieceDistance
        velocity = position - self.last_position
        ind = self.positions[self.name].pieceIndex
        print("%d,%f,%d" % (self.positions[self.name].pieceIndex,
            self.positions[self.name].inPieceDistance,
            game_tick))
        straight_cur = self.track.is_straight(ind)
        straight_next = self.track.is_straight((ind + 1) % self.track.npieces)
        straight_aft_next = self.track.is_straight((ind + 2) % self.track.npieces)

        if ind == 0:
            self.cur_throttle = 1

        if velocity < 6.375: # best for outer: 6.4375
            self.cur_throttle += 0.05
        else:
            self.cur_throttle -= 0.05
            
        if self.cur_throttle < 0:
            self.cur_throttle = 0
        if self.cur_throttle > 1:
            self.cur_throttle = 1
        self.throttle(self.cur_throttle)

        r_ind = self.track.next_round_piece(ind)
        
        if self.track.pieces[r_ind].angle > 0:
            self.switch_lane("Left")
        else:
            self.switch_lane("Right")
        
        
        #if straight_cur and straight_next and straight_aft_next:
        #    self.throttle(1)
        #elif straight_cur and straight_next:
        #    self.throttle(0.7)
        #elif straight_cur:
        #    self.throttle(0.3)
        #elif not straight_cur and not straight_next:
        #    self.throttle(0.5)
        #else:
        #    self.throttle(1)



        self.last_tick = game_tick
        self.last_position = position

    def on_crash(self, data, **kwargs):
        print("Someone crashed")
        raise ""
        self.ping()

    def on_game_end(self, data, **kwargs):
        print("Race ended")
        print(data["results"])
        self.ping()

    def on_error(self, data, **kwargs):
        print("Error: {0}".format(data))
        self.ping()


    def on_game_init(self, msgType, data, **argv):
        print("Game init")
        self.race = data["race"]
        self.track = Track(self.race["track"])
        self.cars = self.race["cars"]
        self.ping()

    def msg_loop(self):
        msg_map = {
            'join': self.on_join,
            'gameStart': self.on_game_start,
            'carPositions': self.on_car_positions,
            'crash': self.on_crash,
            'gameEnd': self.on_game_end,
            'error': self.on_error,
            'gameInit': self.on_game_init
        }
        socket_file = s.makefile()
        line = socket_file.readline()
        while line:
            msg = json.loads(line)
            msg_type, data = msg['msgType'], msg['data']
            if msg_type in msg_map:
                msg_map[msg_type](**msg)
            else:
                print("Got {0}".format(msg_type))
                #if msg_type == "gameInit":
                #    print(data)
                self.ping()
            line = socket_file.readline()


if __name__ == "__main__":
    if len(sys.argv) != 5:
        print("Usage: ./run host port botname botkey")
    else:
        host, port, name, key = sys.argv[1:5]
        print("Connecting with parameters:")
        print("host={0}, port={1}, bot name={2}, key={3}".format(*sys.argv[1:5]))
        s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        s.connect((host, int(port)))
        bot = DecentBot(s, name, key)
        bot.run()
